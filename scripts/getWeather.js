// F U N C T I O N S

// setting up two-dimensional context for canvas element in which the bar chart will be displayed
const getContext2D = elementID => {
    let context = document.getElementById(elementID).getContext('2d');
         console.log('2D context for ' + elementID + ' has been set!' )
         return context;
}

// getting user input from the 'Enter city name' search box
const getInput = () => document.getElementById('search_box').value;

// clearing user input from the 'Enter city name' search box
 const clearInput = () => document.getElementById('search_box').value = '';

// giving focus to a specified html element
 const getFocus = (focusedElement) => {
         console.log(`Element ${focusedElement} is focused now.`);
         return  document.getElementById(focusedElement).focus();
}

//  controls the whole process of searching, requesting and processing the weather data
const controlSearch = async () => {
    const query = getInput(); // getting user input from the 'Enter city name' search box and storing it in a variable

    clearInput(); // clering user input from the 'Enter city name' search box

    if(query) { // if input exists
        state.search = new Search(query, 'temperature-1', 'wind-1'); // creates new Search object and stores it in the state.search object with given arguments as properties
        await state.search.getWeather(); // calls getWeather method from its prototype
        console.log(state);
        getFocus('btn-board'); // // giving focus to 'on board' button

        // setting up event listener for 'on board' button
        // when button is clicked, renderNewItem method (creation of the widget) from the prototype of the latest search object is called
        document.getElementById('btn-board').addEventListener('click', controlNewItem);
    }
}


const controlNewItem = () => {
    if (!state.items) state.items = new NewItems();
    const currentID = state.search.id;
    if (!document.getElementById(currentID))  {
        if (currentID != undefined) {
    const newItem = state.items.addNewItem(currentID, state.search.city, state.search.country, state.search.temperature, state.search.windSpeed);
    console.log(state.items);

    renderNewItems(newItem);
    

    evtListener(currentID);
        }
    }
}

// Event listener for new items
const evtListener = (ids) => {
    const widget = document.querySelector('.list');
    if (widget) widget.addEventListener('click', event => {
        if (event.target.parentElement.id == ids && event.target.id == 'cancel') {
            deleteUI(ids);
            state.items.deleteNewItem(ids);
            console.log(state.items);
        }
    })
}


// Restore new items on page load
window.addEventListener('load', () => {
    state.items = new NewItems();

    // Restore new items
    state.items.readStorage();

    // Render the existing new items
    state.items.newItems.forEach(element => renderNewItems(element));

    if (document.querySelector('.new_item')) {
    document.querySelector('.list').addEventListener('click', e => {
                let tgtID = e.target.id;
                let tgtParentID = e.target.parentElement.id;
                for (i = 0; i < state.items.newItems.length; i++) {
                let ID = state.items.newItems[i].id;
                    if (tgtParentID == ID && tgtID == 'cancel' ) {

                        const deleteNI = (id) => {
                            const index = state.items.newItems.findIndex(el => el.id == id);
                            state.items.newItems.splice(index, 1);
                        };
                        deleteNI(tgtParentID);
                        deleteUI(ID);
                        persistData2();
                    console.log(state.items);
                }
            }
        })
}
    const persistData2 = () => {
        localStorage.setItem('items', JSON.stringify(state.items.newItems));
    }
    

})

const deleteUI = id => {
    const el = document.getElementById(id);
    if (el) el.parentElement.removeChild(el);

}
// Creates small weather widgets containing bar chart and weather data to be displayed on the 'weather board'
const renderNewItems = item => {
    if (item.id !== undefined) {
    const markup = `
                <li class="new_item" id="${item.id}">
                    <button class="cancel" id="cancel">x</button>
                    <h2 class="city">${item.city}, <span>${item.country}</span></h2>
                    <div class="wrapper2">
                        <div id="temperature-${item.id}" class="temperature-text2">0&#8451</div>
                        <div class="container widget-chart">
                            <canvas class="myChart2" id="canvas-${item.id}">${item.chartContext}</canvas>
                        </div>
                        <div id="wind-${item.id}" class="wind-text2">0 km/h</div>
                    </div>
                </li>
        `;
        document.querySelector('.list').insertAdjacentHTML('beforeend', markup); // adding weather widget html structure into main index.html structure

        let ctxSmall = getContext2D(`canvas-${item.id}`); // setting 2d context for the weather widget canvas element and storing it in a variable

        Chart.defaults.global.responsive = true; //makes all charts responsive

        // creating new instance of Chart class using Chart.js library and storing it in state.charts object.
        // It will be used for each small widget chart displayed on the 'weather board' (right side of the web page)
        state.charts = new Chart(ctxSmall, {
            type: 'bar',
            data: { // chart data
                labels: [''],
                datasets: [{
                    label: ['Temperature'],
                    data: [''],
                    backgroundColor: ['rgb(247, 159, 31)']
                    },
                    {
                    label: 'Wind Speed',
                    data: [''],
                    backgroundColor: ['#2980b9']
                    }
                ]
            },
            options: { // chart options
                maintainAspectRatio: false,
                tooltips: {enabled: true},
                title: {display:false},
                legend: {display: true},
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false
                        },
                        barPercentage: .5},
                    ],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            max: 40,
                            min:-20,
                            stepSize: 5,
                            },
                        gridLines: {}
                    }]
                }
            }
        });

        this.temperature =  item.temperature;  // setting temperature value from the latest searched data response
        state.charts.data.datasets[0].data[0] = this.temperature; // setting temperature value in the widget bar chart

        this.windSpeed = item.windSpeed;  // setting wind speed value from the latest searched data response
        state.charts.data.datasets[1].data[0] = this.windSpeed; // setting wind speed value in the widget bar chart

        state.charts.update(); // updating the chart with new values

        // setting temperature value to be displayed on the left side of the widget chart with the degree Celsius symbol
        document.getElementById(`temperature-${item.id}`).innerHTML = item.temperature + '&#8451';

        // setting wind speed value to be displayed on the right side of the widget chart
        document.getElementById(`wind-${item.id}`).innerHTML = item.windSpeed + ' kph';

        addedCharts.push(state.charts); // adding the latest Chart object into addedItems array

        getFocus('search_box'); //giving focus to 'Enter city name' search box
    }
}

// V A R I A B L E S

const state = {}; // stores data returned by current fetch request (current search) in an object

const addedItems = []; // stores array of data that are returned by fetch request and added to the 'weather board' on the right side of the web page
const addedCharts = []; // stores array of charts data added to the 'weather board' on the right side of the web page

const ctx = getContext2D('my-chart-main'); // setting 2d context for the main canvas element (left side of the web page) and storing it in a variable

// creating new instance of Chart class using Chart.js library. It will be the main chart displayed on the left side of the web page
const myChart = new Chart(ctx, {
            type: 'bar',
            data: { // chart data
                labels: [''],
                datasets: [{
                    label: ['Temperature'],
                    data: [''],
                    backgroundColor: ['rgb(247, 159, 31)']
                    },
                    {
                    label: 'Wind Speed',
                    data: [''],
                    backgroundColor: ['#2980b9']
                    }
                ]
            },
            options: { //chart options
                maintainAspectRatio: false,
                tooltips: {enabled: true},
                title: {display:false},
                legend: {display: true},
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                        },
                        barPercentage: .5,
                        categoryPercentage: .7
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            max: 40,
                            min:-20,
                            stepSize: 5,
                            },
                        gridLines: {}
                    }]
                }
            }
});

Chart.defaults.global.responsive = true; // global setting that makes all charts responsive


// C L A S S E S

class NewItems {
    constructor() {
        this.newItems = [];
    }

    addNewItem (id, city, country, temperature, windSpeed) {
        const item = {id, city, country, temperature, windSpeed};
        this.newItems.push(item);

        // persist data in local storage
        this.persistData();

        return item;
    }

    deleteNewItem (id) {
        const index = this.newItems.findIndex(el => el.id == id);
        this. newItems.splice(index, 1);

        // persist data in local storage
        this.persistData();
    }

    // persisting data in localStorage
    persistData () {
        localStorage.setItem('items', JSON.stringify(this.newItems));
    }

    // restoring items from the localStorage
    readStorage () {
        const storage = JSON.parse(localStorage.getItem('items'));
        if (storage) {this.newItems = storage;} 
    }
}


class Search {
        constructor(query, temperatureElementID, windElementID) {
            this.query = query; // search word as an input from the 'Enter city name' search box
            this.temperatureElementID = temperatureElementID; // div element in which temperature value will be displayed on the left side of the main bar chart
            this.windElementID = windElementID; // div element in which wind speed value will be displayed on the right side of the main bar chart

        }
        // Search class method for getting and processing weather data from Open Weather Map API resource
        getWeather() {
            const url = 'http://api.openweathermap.org/data/2.5/weather'; // Open Weather Map API url
            const APIkey = '8a8b25d317fb33f796fdcdefb6657cd0';
            const units = 'metric';

            fetch(`${url}?q=${this.query}&APPID=${APIkey}&units=${units}`)  // fetch request
            .then(res => {console.log(res); return res.json()}) // returning promise and getting response body and parsing it as JSON object
            .then(data => {
                console.log(data);

                // saving received data into search object properties:
                this.city =  data.name; 

                this.country = data.sys.country;

                this.id = data.id; // unique city ID

                this.temperature = Math.round(data.main.temp);
                myChart.data.datasets[0].data[0] = this.temperature; // setting temperature value in the main bar chart

                this.windSpeed = Math.round(data.wind.speed * 3.6); // changing the speed value from m/s to km/h
                myChart.data.datasets[1].data[0] = this.windSpeed; // setting wind speed value in the main bar chart

                myChart.update(); // updating the chart with new values

                const countr = `<span>${data.sys.country}</span>`; // creating span element with country value for styling purposes

                // setting temperature value to be displayed on the left side of the main chart with the degree Celsius symbol
                document.getElementById(this.temperatureElementID).innerHTML = this.temperature + '&#8451';

                document.getElementById(this.windElementID).innerHTML = this.windSpeed + ' kph'; // setting wind speed value to be displayed on the right side of the main chart
                document.querySelector('.city').innerHTML = `${this.city}, `; // setting the searched city name to be displayed above the main chart
                document.querySelector('.city').insertAdjacentHTML('beforeend', countr); // setting the country value to be displayed after the city name
            })
            .catch(error => {
                console.log(error);
                alert(error);
            });
        }
}


//  E V E N T   L I S T E N E R S

// Setting up event listener for search button
document.getElementById('btn-kit').addEventListener('click', controlSearch);
document.getElementById('search_box').addEventListener('keyup', (event) => {
    if (event.keyCode === 13) {
        controlSearch();
    }
    });

    
/*function getUpdate () {
    setInterval(() => {
        for(let i = 0; i < addedItems.length; i++) {
            if (addedItems[i].id === idecko) {
                console.log('yes');
            };
        }
    }, 3000);
}
var idecko = document.querySelector('.new_item').id;
*/